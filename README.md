# NextCherry 基于事件的响应式设备联动框架

## 目录结构

- `core` NextCherry的架构实现；
- `raspberry` 基于树莓派 Pi4J 驱动库的设备对象库；
- `demo-client` 跨节点演示项目-客户端；
- `demo-server` 跨节点演示项目-服务端；

##
