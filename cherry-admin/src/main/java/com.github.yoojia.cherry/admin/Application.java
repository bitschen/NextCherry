package com.github.yoojia.cherry.admin;

import com.github.yoojia.cherry.admin.apis.AccessAuthInterceptor;
import com.github.yoojia.cherry.admin.apis.AdminLoginHandler;

import static spark.Spark.*;
/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class Application {

    public static void main(String[] args) {
        // 登录后台授权验证
//        staticFileLocation("/public");
        externalStaticFileLocation("src/main/resources/public/");

        before(new AccessAuthInterceptor());
        // 后台登录
        post("/api/admin", new AdminLoginHandler());

    }
}
