package com.github.yoojia.cherry.admin.apis;

import spark.Filter;
import spark.Request;
import spark.Response;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class AccessAuthInterceptor implements Filter{

    @Override
    public void handle(Request request, Response response) throws Exception {

    }
}
