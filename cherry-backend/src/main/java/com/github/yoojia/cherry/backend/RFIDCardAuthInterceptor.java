package com.github.yoojia.cherry.backend;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.core.AuthService;
import com.github.yoojia.cherry.support.CheckedEventInterceptor;
import com.github.yoojia.cherry.value.StringValue;
import com.parkingwang.lang.data.Config;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class RFIDCardAuthInterceptor extends CheckedEventInterceptor {

    private final AuthService mAuthService = new AuthService();

    @Override
    public void onInit(Context context, Config args) throws Exception {
        super.onInit(context, args);
        mAuthService.init();
    }

    @Override
    protected boolean onCheck(Event event) {
        return event.isTargetMatch(getArgs().getString("topic-filter"));
    }

    @Override
    protected boolean handleEvent(Context context, Event event) {
        final StringValue sv = event.valueOfString();
        return mAuthService.check(sv.getText());
    }
}
