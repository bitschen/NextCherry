package com.github.yoojia.cherry.core;

import com.github.yoojia.cherry.core.db.DBAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class AuthService {

    private final static Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

    private static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS t_authid (auth_id TEXT PRIMARY KEY NOT NULL);";
    private static final String SQL_CHECK = "SELECT * FROM t_authid WHERE auth_id=?;";
    private static final String SQL_INSERT = "INSERT INTO t_authid SET(auth_id=?);";

    public void init(){
        DBAdapter.use(conn -> {
            Statement stm = conn.createStatement();
            stm.execute(SQL_CREATE_TABLE);
            stm.close();
            return true;
        });
    }

    public boolean check(String authId){
        return DBAdapter.call(false, conn -> {
            final PreparedStatement stm = conn.prepareStatement(SQL_CHECK);
            stm.setString(1, authId);
            final ResultSet rs = stm.executeQuery();
            try{
                return rs.next();
            }finally {
                stm.close();
                rs.close();
            }
        });
    }

}
