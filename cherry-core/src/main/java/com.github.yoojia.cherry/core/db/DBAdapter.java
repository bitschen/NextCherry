package com.github.yoojia.cherry.core.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class DBAdapter {

    private final static Logger LOGGER = LoggerFactory.getLogger(DBAdapter.class);

    private static final String DB_NAME = "cherry.core.db";

    private static final int MAX_CONNECTION = 2;

    private static final BlockingQueue<Connection> IDLE_CONN_POOL = new ArrayBlockingQueue<>(MAX_CONNECTION);

    private DBAdapter(){}

    /**
     * 初始化数据库
     */
    public static void init(){
        if (IDLE_CONN_POOL.size() == MAX_CONNECTION){
            throw new IllegalStateException("Database Adapter was Initialized");
        }
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        // 初始化数据库连接池
        try{
            for (int i = 0; i < MAX_CONNECTION; i++) {
                IDLE_CONN_POOL.put(DriverManager.getConnection("jdbc:sqlite:" + DB_NAME));
            }
        }catch (Exception e){
            LOGGER.error("Error when INIT Database connection", e);
        }
    }

    public static void destroy(){
        IDLE_CONN_POOL.forEach(conn-> {
            try{
                conn.close();
            }catch (Exception e){
                LOGGER.error("Error when close Database connection", e);
            }
        });
    }

    /**
     * 使用空闲的数据库连接来执行任务
     * @param consumer 使用数据库连接的任务接口
     */
    public static void use(ThrowsConsumer consumer){
        try {
            final Connection conn = getIdle();
            try{
                consumer.accept(conn);
            }finally {
                release(conn);
            }
        }catch (Exception e){
            LOGGER.error("Error when using Database connection", e);
        }
    }

    /**
     * 使用空闲的数据库连接来执行任务，并返回值。如果发生错误，返回指定的默认值。
     * @param defReturn 发生错误时返回指定的默认值
     * @param consumer 使用数据库连接的任务接口
     * @param <T> 返回类型
     * @return 返回值
     */
    public static <T> T call(T defReturn, ThrowsConsumer<T> consumer){
        try {
            final Connection conn = getIdle();
            try{
                return consumer.accept(conn);
            }finally {
                release(conn);
            }
        }catch (Exception e){
            LOGGER.error("Error when using Database connection", e);
            return defReturn;
        }
    }

    ////

    private static void release(Connection connection) throws Exception{
        if (connection.isClosed()){
            throw new IllegalStateException("Database Connection CAN NOT close manually!");
        }
        IDLE_CONN_POOL.put(connection);
    }

    private static Connection getIdle() throws Exception {
        return IDLE_CONN_POOL.take();
    }

}
