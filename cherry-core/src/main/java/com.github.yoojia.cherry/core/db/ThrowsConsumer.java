package com.github.yoojia.cherry.core.db;

import java.sql.Connection;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
@FunctionalInterface
public interface ThrowsConsumer<T> {

    T accept(Connection conn) throws Exception;

}
