package com.github.yoojia.cherry.pi;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.Plugin;
import com.github.yoojia.cherry.value.BoolValue;
import com.parkingwang.lang.data.Config;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class LEDBlinkPlugin implements Plugin {

    private final Timer mTimer = new Timer("LEDBlink", true);

    private Context mContext;

    @Override
    public void onInit(Context context, Config args) {
        mContext = context;
    }

    @Override
    public void onStart() {
        mTimer.scheduleAtFixedRate(new TimerTask() {

            private boolean mStateON = false;

            @Override
            public void run() {
                mStateON = !mStateON;
                Event blink = Event.create(
                        BoolValue.create(mStateON).getBytes(),
                        Context.TOPIC_SWITCH,
                        "plugin",
                        "raspberry.output.led#0");
                mContext.postEvent(blink);
                System.out.println("Blink send event...");
            }
        }, 1000, 10000);
    }

    @Override
    public void onStop() {
        mTimer.purge();
        mTimer.cancel();
    }
}
