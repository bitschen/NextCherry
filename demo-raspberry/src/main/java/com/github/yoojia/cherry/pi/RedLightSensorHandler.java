package com.github.yoojia.cherry.pi;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.support.RemoteEventHandler;
import com.github.yoojia.cherry.value.BoolValue;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class RedLightSensorHandler extends RemoteEventHandler {

    @Override
    protected boolean check(Event event) {
        return super.check(event)
                && event.isSourceMatch("raspberry.output.input#21");
    }

    @Override
    protected void handleEvent(Context context, Event event) {
        final BoolValue sensorValue = event.valueOfBool();
        System.err.println("# 收到传感器状态： " + (sensorValue.bool ? "ON" : "OFF"));
        final Event led = Event.create(
                BoolValue.create(sensorValue.bool).getBytes(),
                Context.TOPIC_SWITCH,
                "redlight-handler",
                "raspberry.output.led#0");
        context.postEvent(led);
    }
}
