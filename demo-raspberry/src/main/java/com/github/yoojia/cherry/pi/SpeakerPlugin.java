package com.github.yoojia.cherry.pi;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.Plugin;
import com.github.yoojia.cherry.value.IntValue;
import com.parkingwang.lang.data.Config;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class SpeakerPlugin implements Plugin {

    private final Timer mTimer = new Timer("Speaker", true);

    private Context mContext;

    @Override
    public void onInit(Context context,Config args) {
        mContext = context;
    }

    @Override
    public void onStart() {
        mTimer.scheduleAtFixedRate(new TimerTask() {

            private boolean mStateON = false;

            @Override
            public void run() {
                mStateON = !mStateON;
                Event blink = Event.create(
                        IntValue.create((mStateON ? 3000 : 0), 5000).getBytes(),
                        Context.TOPIC_SWITCH,
                        "plugin",
                        "raspberry.output.speaker#1");
                mContext.postEvent(blink);
                System.out.println("Speaker send event...");
            }
        }, 1000, 3000);
    }

    @Override
    public void onStop() {
        mTimer.purge();
        mTimer.cancel();
    }
}
