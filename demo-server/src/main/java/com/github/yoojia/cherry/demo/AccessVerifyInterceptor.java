package com.github.yoojia.cherry.demo;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.EventInterceptor;
import com.parkingwang.lang.data.Config;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class AccessVerifyInterceptor implements EventInterceptor{

    @Override
    public void onInit(Context context, Config args) {

    }

    @Override
    public boolean onEvent(Context context, Event event) {
        return true;
    }
}
