package com.github.yoojia.cherry.demo;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.support.CheckedEventHandler;
import com.github.yoojia.cherry.support.VirtualSchedulerDevice;
import com.github.yoojia.cherry.value.BoolValue;
import com.github.yoojia.cherry.value.IntValue;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class DevicesTriggerHandler extends CheckedEventHandler{

    @Override
    protected boolean check(Event event) {
        // 接收Switch事件
        return event.isTopicMatch(VirtualSchedulerDevice.TOPIC);
    }

    @Override
    protected void handleEvent(Context context, Event event) {
        System.err.println("执行器收到消息: " + event);
        final IntValue evt = event.valueOfInt();
        final Event reply = Event.createRemoteTarget(
                BoolValue.create(evt.int1 >= 300).getBytes(),
                Context.TOPIC_SWITCH,
                event.sourceAddress,
                "755.client.output");
        System.err.println("执行器响应: " + reply);
        context.postEvent(reply);
    }
}
