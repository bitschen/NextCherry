package com.github.yoojia.cherry;


import com.parkingwang.lang.Required;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public abstract class AbstractVirtualDevice extends InitialContext implements VirtualDevice {

    private final Required<String> mUUID = new Required<>();

    @Override
    final public void setUUID(String uuid) {
        mUUID.set(uuid);
    }

    @Override
    final public String getUUID() {
        return mUUID.getChecked();
    }

    @Override
    public void onStart() {}

    @Override
    public void onStop() {}

    protected void postEvent(Event event){
        getContext().postEvent(event);
    }

    protected boolean isLogging(){
        return getArgs().getBoolean("debug", false);
    }

    protected String msgWithId(String text){
        return text + "@" + getUUID();
    }

    ////

    protected static String createTopic(String name){
        return Context.TOPIC_PREFIX + name;
    }

    protected static String topicAppend(String topic, String seg){
        return topic + "/" + seg;
    }

}
