package com.github.yoojia.cherry;

import com.github.yoojia.cherry.util.AppPaths;
import com.github.yoojia.cherry.util.Classes;
import com.github.yoojia.cherry.util.ConfigKit;
import com.parkingwang.lang.Try;
import com.parkingwang.lang.data.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class Application implements Context {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private final List<EventInterceptor> mInterceptors = new ArrayList<>();
    private final List<EventProcessor> mProcessors = new ArrayList<>();
    private final List<EventHandler> mHandlers = new ArrayList<>();
    private final List<Plugin> mPlugins = new ArrayList<>();

    private final List<VirtualDevice> mDevices = new ArrayList<>();
    private final Map<String, Integer> mUUIDIndex = new HashMap<>();

    private final Config mGlobalConfig = new Config();

    // 事件路由
    private final Dispatcher mDispatcher = new Dispatcher(mInterceptors, mProcessors, mHandlers, mDevices);

    //// Context Impl

    @Override
    public void postEvent(Event event) {
        mDispatcher.enqueueEvent(event);
    }

    @Override
    public VirtualDevice findLocalDeviceByUUID(String uuid) {
        if (mUUIDIndex.containsKey(uuid)){
            return mDevices.get(mUUIDIndex.get(uuid));
        }else{
            return null;
        }
    }

    @Override
    public Config getRootConfig() {
        return mGlobalConfig;
    }

    @Override
    public Set<String> getDeviceUUIDs() {
        return mUUIDIndex.keySet();
    }

    ////

    /**
     * 以默认配置文件执行初始化操作。
     * 默认配置文件在程序目录下的 cherry-config.yml 文件。
     */
    public void initWithDefaultConfig(){
        // 启动时，加载配置文件：
        final File file = AppPaths.pathOf("cherry-config.yml").toFile();
        if (!file.exists()){
            ConfigKit.extractByName("/cherry-config.sample.yml", "cherry-config.yml");
        }
        initWithFile(file);
    }

    /**
     * 以指定配置文件执行初始化操作。
     */
    public void initWithFile(File configFile){
        LOGGER.info("Init from config: {}", configFile.getAbsolutePath());
        initWithConfig(Try.die(()-> {
            try(InputStream inputStream = new FileInputStream(configFile)){
                return ConfigKit.stream(inputStream);
            }
        }));
    }

    /**
     * 以指定配置对象执行初始化操作。
     */
    public void initWithConfig(Config config){
        mGlobalConfig.copyFrom(config);

        //// 加载核心组件 /////

        mDispatcher.onInit(this, takeConfigFrom(mGlobalConfig,"mqtt-connection"));

        mPlugins.forEach(plugin ->
                initOrExit(plugin, this, ConfigKit.empty()));
        addInitComponents("plugins");

        mInterceptors.forEach(interceptor ->
                initOrExit(interceptor, this, ConfigKit.empty()));
        addInitComponents("interceptors");

        addComponents("processors");

        addComponents("handlers");

        mDevices.forEach(device ->
                initOrExit(device, this, ConfigKit.empty()));
        addInitComponents("devices");

        LOGGER.info("Init loaded Plugins: {}", mPlugins.size());
        LOGGER.info("Init loaded Interceptors: {}", mInterceptors.size());
        LOGGER.info("Init loaded Processors: {}", mProcessors.size());
        LOGGER.info("Init loaded Handlers: {}", mHandlers.size());
        LOGGER.info("Init loaded Devices: {}", mDevices.size());
    }

    public void start(){
        LOGGER.debug("START...");
        // Starts
        mDispatcher.onStart();
        mPlugins.forEach(Plugin::onStart);
        mDevices.forEach(VirtualDevice::onStart);
    }

    public void stop(){
        LOGGER.debug("STOP...");
        // Stops
        mDevices.forEach(VirtualDevice::onStop);
        mPlugins.forEach(Plugin::onStop);
        mDispatcher.onStop();
    }

    ////

    /**
     * 安装事件转换处理器
     * @param processor EventProcessor
     */
    public Application install(EventProcessor processor){
        mProcessors.add(processor);
        return this;
    }

    /**
     * 安装事件拦截处理器
     * @param interceptor EventInterceptor
     */
    public Application install(EventInterceptor interceptor){
        mInterceptors.add(interceptor);
        return this;
    }

    /**
     * 安装事件动作执行器
     * @param handler EventHandler
     */
    public Application install(EventHandler handler){
        mHandlers.add(handler);
        return this;
    }

    /**
     * 安装插件
     * @param plugin Plugin
     */
    public Application install(Plugin plugin){
        mPlugins.add(plugin);
        return this;
    }

    /**
     * 安装虚拟设备
     * @param device VirtualDevice
     */
    public Application register(VirtualDevice device){
        final String uuid = device.getUUID();
        if (mUUIDIndex.containsKey(uuid)){
            throw new IllegalArgumentException("UUID for device MUST BE Unique，duplicated：" + uuid);
        }
        mDevices.add(device);
        final int index = mDevices.size() - 1;
        mUUIDIndex.put(uuid, index);
        return this;
    }

    ////

    private void addInitComponents(String configName){
        enabledConfigList(configName).forEach(config -> {
            final Object object = Classes.newInstance(config.getString("class"));
            final Class<?> type = object.getClass();
            if (! (object instanceof Initial)){
                throw new IllegalArgumentException("Object is NOT <Initial>, was: " + type);
            }
            // EventInterceptor
            if (object instanceof EventInterceptor){
                registerLogging("EventInterceptor", type);
                install((EventInterceptor) object);
            }else
            // VirtualDevice
            if (object instanceof VirtualDevice){
                ConfigKit.checkRequired(config, "uuid", "Config<uuid> is REQUIRED for Devices");
                final VirtualDevice device = (VirtualDevice) object;
                device.setUUID(config.getString("uuid"));
                registerLogging("VirtualDevice", type);
                register(device);
            }else
            // Plugin：由于Device继承了Plugin接口，Plugin的判断需要放在Device后面
            if (object instanceof Plugin){
                registerLogging("Plugin", type);
                install((Plugin) object);
            }else{
                throw new IllegalArgumentException("Object is NOT <InitialComponent>, was: " + type);
            }
            // Init
            initOrExit((Initial) object, this, takeConfigFrom(config, "args"));
        });
    }

    private void addComponents(String configName){
        enabledConfigList(configName).forEach(config -> {
            final Object object = Classes.newInstance(config.getString("class"));
            final Class<?> type = object.getClass();
            if (object instanceof EventProcessor){
                registerLogging("EventProcessor", type);
                install((EventProcessor) object);
            }else if (object instanceof EventHandler){
                registerLogging("EventHandler", type);
                install((EventHandler) object);
            }else{
                throw new IllegalArgumentException("Object is NOT cherry components, was: " + object.getClass());
            }
        });

    }

    private Stream<Config> enabledConfigList(String name){
        return popConfigList(name).stream()
                .filter(config ->
                        config.getBoolean("enabled", true))
                .map(config-> {
                    ConfigKit.checkRequired(config, "class", "Components");
                    config.getDataMap().put("class", Classes.resolveImplClass(config.getString("class")));
                    return config;
                });
    }

    private List<Config> popConfigList(String configName){
        try{
            return mGlobalConfig.getConfigList(configName);
        }finally {
            mGlobalConfig.getDataMap().remove(configName);
        }
    }

    private Config takeConfigFrom(Config source, String configName){
        try{
            return source.getConfig(configName);
        }finally {
            // 节点配置信息，在初始化完成后，清除内存中的配置数据
            source.getDataMap().remove(configName);
        }
    }

    private static void initOrExit(Initial initial, Context context, Config config){
        try {
            initial.onInit(context, config);
        } catch (Exception e) {
            LOGGER.error("Error when initializing object: " + initial, e);
            System.exit(-1);
        }
    }

    private static void registerLogging(String typeName, Class<?> object){
        LOGGER.info("Register from config: {}, instance: {}", typeName, object);
    }
}
