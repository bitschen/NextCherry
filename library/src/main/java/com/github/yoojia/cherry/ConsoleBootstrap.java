package com.github.yoojia.cherry;

import com.github.yoojia.cherry.util.Console;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class ConsoleBootstrap {

    public static void main(String[] args) throws InterruptedException {
        Console console = new Console();
        console.promptForExit();
        Application engine = new Application();
        engine.initWithDefaultConfig();
        engine.start();
        try{
            console.waitForExit();
        }finally {
            engine.stop();
        }
    }
}
