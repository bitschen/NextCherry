package com.github.yoojia.cherry;

import com.parkingwang.lang.data.Config;

import java.util.Set;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface Context {

    String TOPIC_PREFIX = "T#";
    String TARGET_REMOTE_PREFIX = "R@";
    String TARGET_NONE = "*";

    int TARGET_REMOTE_INDEX = TARGET_REMOTE_PREFIX.length();

    String TOPIC_READER = TOPIC_PREFIX + "readers";
    String TOPIC_SWITCH = TOPIC_PREFIX + "switches";
    String TOPIC_SENSOR = TOPIC_PREFIX + "sensors";

    /**
     * 发送事件
     * @param event Event
     */
    void postEvent(Event event);

    /**
     * 按设备UUID来查找设备对象
     * @param uuid 设备UUID
     * @return 匹配的设备对象。否则返回 null。
     */
    VirtualDevice findLocalDeviceByUUID(String uuid);

    /**
     * 获取根配置对象
     * @return Config
     */
    Config getRootConfig();

    /**
     * @return 本地设备的UUID列表
     */
    Set<String> getDeviceUUIDs();
}
