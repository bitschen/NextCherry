package com.github.yoojia.cherry;

import com.github.yoojia.cherry.util.ConfigKit;
import com.github.yoojia.mqtt.MQTTSocket;
import com.github.yoojia.mqtt.Message;
import com.github.yoojia.mqtt.MessageHandler;
import com.parkingwang.lang.Lazy;
import com.parkingwang.lang.Required;
import com.parkingwang.lang.data.Config;
import com.parkingwang.lang.kit.CollectionKit;
import com.squareup.moshi.Moshi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 负责消息路由。
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
class Dispatcher implements Lifecycle {

    private static final Logger LOGGER = LoggerFactory.getLogger(Dispatcher.class);

    private static final String TAG_MQTT = "MQTTConnection";

    private static final String KEY_ADDRESS = "address";
    private static final String KEY_DOMAIN = "domain";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_LOCAL_ID = "local-node-id";
    private static final String KEY_REMOTE_ID = "remote-node-id";

    private static final Set<String> KEY_CONFS = CollectionKit.hashSetOf(KEY_ADDRESS, KEY_DOMAIN, KEY_LOCAL_ID, KEY_REMOTE_ID);

    private static final int EVENT_QUEUE_CAPACITY = 10;

    private final List<EventInterceptor> mInterceptors;
    private final List<EventProcessor> mProcessors;
    private final List<EventHandler> mHandlers;
    private final List<VirtualDevice> mLocalDevices;

    private final BlockingQueue<Event> mLocalSendingQueue = new LinkedBlockingQueue<>(EVENT_QUEUE_CAPACITY);
    private final BlockingQueue<Event> mRemoteSendingQueue = new LinkedBlockingQueue<>(EVENT_QUEUE_CAPACITY);

    private final ExecutorService mEventLoopThreads = Executors.newFixedThreadPool(2);

    private final Config mConfig = new Config();

    private Context mContext;

    // 双事件缓存
    private final Runnable mLocalEventLoop = new EventLooper(mLocalSendingQueue);
    private final Runnable mRemoteEventLoop = new EventLooper(mRemoteSendingQueue);

    private final Lazy<MQTTSocket> mMQTTSocket = Lazy.from(()->
            MQTTSocket.context()
                    .address(mConfig.getString(KEY_ADDRESS))
                    .autoReconnect(true)
                    .clearSession(true)
                    .domain(mConfig.getString(KEY_DOMAIN, "NEXT-CHERRY"))
                    .nodeId(mConfig.getString(KEY_LOCAL_ID))
                    .username(mConfig.getString(KEY_USERNAME))
                    .password(mConfig.getString(KEY_PASSWORD))
                    .socket()
    );

    private final Moshi mJSONSerialize = new Moshi.Builder().build();

    /**
     * 接收远程服务器的事件消息。
     */
    private final MessageHandler mRemoteEventReceiver = (socket, message) -> {
        final Event rawEvent;
        try {
            rawEvent = mJSONSerialize
                    .adapter(Event.class)
                    .fromJson(message.getStringPayload());
        } catch (IOException e) {
            LOGGER.error("Error when Serializing MQTT message to Event", e);
            LOGGER.error(message.getStringPayload());
            return;
        }
        // 接收远程事件，需要解除RemoteTarget地址前缀，以转达到本地设备。
        // 同时标记事件从MQTT远程节点接收的其它选项信息。
        final Event outEvent = Event.unwrapRemoteTarget(rawEvent);
        Event.Options opts = outEvent.options();
        opts.mqttRemote(true)
                .mqttQos(message.qos)
                .mqttRetained(message.retained);
        mContext.postEvent(outEvent);
    };

    ////

    Dispatcher(List<EventInterceptor> interceptors,
               List<EventProcessor> processors,
               List<EventHandler> handlers,
               List<VirtualDevice> devices) {
        mInterceptors = interceptors;
        mProcessors = processors;
        mHandlers = handlers;
        mLocalDevices = devices;
    }

    @Override
    public void onInit(Context context, Config args) {
        mContext = context;
        mConfig.copyFrom(args);
    }

    @Override
    public void onStart(){
        // Start loop
        mEventLoopThreads.submit(mLocalEventLoop);
        mEventLoopThreads.submit(mRemoteEventLoop);

        // 在配置了服务器信息的情况下启用MQTT远程事件
        if (mConfig.getBoolean("enabled", true) &&
                mConfig.containsNames(KEY_CONFS)){
            ConfigKit.checkNotEmpty(mConfig, KEY_ADDRESS, TAG_MQTT);
            ConfigKit.checkNotEmpty(mConfig, KEY_LOCAL_ID, TAG_MQTT);
            ConfigKit.checkNotEmpty(mConfig, KEY_REMOTE_ID, TAG_MQTT);
            final String localId = mConfig.getString(KEY_LOCAL_ID);
            final String remoteId = mConfig.getString(KEY_REMOTE_ID);
            if (localId.equals(remoteId)){
                throw new IllegalArgumentException("MQTT config<local-node-id> & <remote-local-id> MUST NOT be same");
            }
            final MQTTSocket socket = mMQTTSocket.getChecked();
            socket.connect();
            // 注册为消息广播接收模式
            socket.addSubMessageHandler(mRemoteEventReceiver);
            LOGGER.info("Connected to MQTT Broker, Local.NodeId: {}, Remote.NodeId: {}", localId, remoteId);
        }else{
            LOGGER.info("!!! Disabled or Key configs<{}> from MQTTSocket is NOT FOUND, MQTT Remote Node is DISABLED.", KEY_CONFS);
        }
    }

    @Override
    public void onStop(){
        mMQTTSocket.ifPresent(MQTTSocket::disconnect);
        mEventLoopThreads.shutdown();
    }

    /**
     * 将事件压入队列。
     * 派发器区分发送给本地设备的事件和发给远程设备的事件，使用两个队列来缓存，以加速本地设备事件的处理速度。
     * @param event Event
     */
    void enqueueEvent(Event event) {
        try {
            if (isAimToRemote(event)){
                mRemoteSendingQueue.put(event);
            }else{
                mLocalSendingQueue.put(event);
            }
        } catch (InterruptedException e) {
            LOGGER.error("Error when put event to queue", e);
        }
    }

    /**
     * 分派消息事件。
     * 消息被分派时进入EventRouter时，都会经过几个步骤处理：
     * 1. Interceptor拦截器组处理，任何一个拦截器拦截后，消息将不再分派。
     * 2. Processor 消息转换处理。
     * 3. 远程事件，由MQTT服务器广播出去
     * 5. 本地事件，由本地事件执行器执行；由本地设备组执行。
     * @param context Context
     * @param rawEvent Event
     */
    private void dispatch(Context context, Event rawEvent){
        // 拦截器处理：
        final boolean passed = mInterceptors.isEmpty() ||
                mInterceptors.stream()
                        .allMatch(interceptor -> {
                            try{
                                return interceptor.onEvent(context, rawEvent);
                            }catch (Throwable e){
                                LOGGER.error("Error when EventInterceptor processing", e);
                                return false;
                            }
                        });

        if (!passed){ return; }

        // 事件处理器进行转换处理：
        final Required<Event> event = new Required<>(rawEvent);
        mProcessors.forEach(processor->
                catchInvoke(()-> {
                    final Event e = processor.process(context, event.getChecked());
                    if (e == null) {
                        throw new IllegalArgumentException("EventProcessor MUST NOT return null Event instance");
                    }
                    event.set(e);
                }, "Error when EventProcessor processing"));
        final Event outEvent = event.getChecked();
        // 由事件执行器来执行相应动作：
        mHandlers.forEach(handler->
                catchInvoke(()->
                        handler.onEvent(context, outEvent), "Error when EventHandler processing"));

        // 指定发给远程目标的事件，由MQTTSocket发送给远程节点:
        if(isAimToRemote(outEvent)){
            routeToRemoteDevices(outEvent);
        }else{
            routeToLocalDevices(context, outEvent);
        }
    }

    private void routeToRemoteDevices(Event event){
        if (mMQTTSocket.isNotPresent()){
            final String error = "Try to send REMOTE events，but MQTT Remote Node was DISABLED";
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }
        mMQTTSocket.ifPresent(socket-> {
            final Event.Options opts = event.options();
            final String text = mJSONSerialize
                    .adapter(Event.class)
                    .toJson(event);
            final Message message = socket.newPubMessageFor(mConfig.getString(KEY_REMOTE_ID), text.getBytes())
                    .builder()
                    .qos(opts.mqttQos())
                    .retained(opts.mqttRetained())
                    .build();
            socket.send(message);
        });
    }

    private void routeToLocalDevices(Context context, Event event){
        // 由本地事件目标设备来执行动作：发送事件消息给设备，指定targetAddress为目标设备的UUID即可。
        mLocalDevices.stream()
                .filter(h ->
                        event.isTargetMatch(h.getUUID()))
                .findFirst().ifPresent(device ->
                    catchInvoke(()->
                            device.handleEvent(context, event),
                            "Error when VirtualDevice processing")
                );
    }

    /**
     * 1. 远程地址，必须还有远程前缀标记；
     * 2. 非从MQTT中接收的消息；
     * @param event Event
     * @return 返回是否是否发送给远程节点的标记
     */
    private boolean isAimToRemote(Event event){
        return event.isRemoteTargetAddress() &&
                !event.options().mqttRemote();
    }

    ////

    private class EventLooper implements Runnable {

        private final BlockingQueue<Event> mQueue;

        private EventLooper(BlockingQueue<Event> queue) {
            mQueue = queue;
        }

        @Override
        public void run() {
            while (true){
                final Event event;
                try {
                    event = mQueue.take();
                } catch (InterruptedException e) {
                    LOGGER.error("Error when take event from queue", e);
                    break;
                }
                dispatch(mContext, event);
            }
        }
    }

    private static void catchInvoke(ThrowFunction function, String error){
        try{
            function.accept();
        }catch (Throwable e){
            LOGGER.error(error, e);
        }
    }

    private interface ThrowFunction {

        void accept() throws Throwable;
    }
}
