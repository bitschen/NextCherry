package com.github.yoojia.cherry;

import com.github.yoojia.cherry.value.*;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class Event {

    /**
     * 事件值
     */
    public final byte[] value;

    /**
     * 事件主题分类
     */
    public final String topic;

    /**
     * 发出此事件的来源地址
     */
    public final String sourceAddress;

    /**
     * 预期接收此事件的目标设备地址
     */
    public final String targetAddress;

    /**
     * 作为远程事件时的配置信息
     */
    private transient Options mOptions;

    public Event(byte[] value,
                 String topic,
                 String sourceAddress,
                 String targetAddress) {
        this.value = value;
        this.topic = topic;
        this.sourceAddress = sourceAddress;
        this.targetAddress = targetAddress;
    }

    public Options options() {
        // 哪怕使用 final 来修饰和初始化，transient修饰的变量也会在序列化时被设置为系统默认值：null, 0, false
        if (mOptions == null) {
            mOptions = new Options();
        }
        return mOptions;
    }

    //// Values

    public BoolValue valueOfBool() {
        return BoolValue.create(value);
    }

    public FloatValue valueOfFloat() {
        return FloatValue.create(value);
    }

    public FrameValue valueOfFrame() {
        return FrameValue.create(value);
    }

    public IntValue valueOfInt() {
        return IntValue.create(value);
    }

    public StringValue valueOfString() {
        return StringValue.create(value);
    }

    ////

    public boolean isTopicMatch(String topic){
        return this.topic.equals(topic);
    }

    public boolean isTargetMatch(String target){
        return this.targetAddress.equals(target);
    }

    public boolean isSourceMatch(String source){
        return this.sourceAddress.equals(source);
    }

    /**
     * @return 返回是否为远程目标地址
     */
    public boolean isRemoteTargetAddress(){
        return isRemoteAddress(this.targetAddress);
    }

    /**
     * 创建Event对象
     * @return Event
     */
    public static Event create(byte[] value, String topic, String source, String target){
        return new Event(value, topic, source, target);
    }

    /**
     * 创建无固定目标的Event对象。
     * @return Event
     */
    public static Event createNoneTarget(byte[] value, String topic, String source){
        return create(value, topic, source, Context.TARGET_NONE);
    }

    /**
     * 创建远程Target地址的Event对象。
     * @return 能被识别为远程Event的对象
     */
    public static Event createRemoteTarget(byte[] value, String topic, String source, String target){
        return create(value, topic, source,
                (isRemoteAddress(target) ? target : Context.TARGET_REMOTE_PREFIX + target));
    }

    /**
     * 封装远程Target地址
     * @param event 需要封装远程Target地址的Event
     * @return Event
     */
    public static Event wrapRemoteTarget(Event event){
        if (isRemoteAddress(event.targetAddress)){
            return event;
        }else{
            return create(event.value, event.topic, event.sourceAddress,
                    Context.TARGET_REMOTE_PREFIX + event.targetAddress);
        }
    }

    /**
     * 解除远程Target地址
     * @param event 需要解除远程Target地址的Event
     * @return Event
     */
    public static Event unwrapRemoteTarget(Event event){
        if (isRemoteAddress(event.targetAddress)){
            return create(event.value, event.topic, event.sourceAddress,
                    event.targetAddress.substring(Context.TARGET_REMOTE_INDEX));
        }else{
            return event;
        }
    }

    private static boolean isRemoteAddress(String address){
        return address.startsWith(Context.TARGET_REMOTE_PREFIX);
    }

    ////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event that = (Event) o;
        return Objects.equals(this.sourceAddress, that.sourceAddress) &&
                Objects.equals(this.targetAddress, that.targetAddress) &&
                Objects.equals(this.topic, that.topic) &&
                Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceAddress, targetAddress, topic, value);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("sourceAddress = " + sourceAddress)
                .add("targetAddress = " + targetAddress)
                .add("topic = " + topic)
                .add("value = " + value)
                .toString();
    }

    //////

    public static class Options {

        private int mQoS = 1;
        private boolean mRetained = true;
        private boolean mRemote = false;

        /**
         * 设置事件对象的MQTT QoS值。当事件通过MQTT发送到远程节点时被使用。
         * @param qos QoS
         * @return Options
         */
        public Options mqttQos(int qos){
            mQoS = qos;
            return this;
        }

        public int mqttQos(){
            return mQoS;
        }

        /**
         * 设置事件对象的MQTT Retained值。当事件通过MQTT发送到远程节点时被使用。
         * @param retained Retained
         * @return Options
         */
        public Options mqttRetained(boolean retained) {
            mRetained = retained;
            return this;
        }

        public boolean mqttRetained() {
            return mRetained;
        }

        /**
         * 当事件对象是从MQTT远程节点中获取时，自动设置此标记位。
         * @param remote 标记位
         * @return Options
         */
        Options mqttRemote(boolean remote) {
            mRemote = remote;
            return this;
        }

        /**
         * @return 返回消息是否为从MQTT远程节点中获取的标记
         */
        public boolean mqttRemote() {
            return mRemote;
        }
    }
}
