package com.github.yoojia.cherry;

/**
 * 事件执行器。
 * 在事件被拦截器、转换器处理后，转到处理执行器来根据事件执行不同动作。
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface EventHandler {

    void onEvent(Context context, Event event);

}
