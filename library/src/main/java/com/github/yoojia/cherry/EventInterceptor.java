package com.github.yoojia.cherry;

/**
 * 事件拦截器。
 * 拦截不合法事件，使得事件在分派前被中断。
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface EventInterceptor extends Initial {

    boolean onEvent(Context context, Event event);

}
