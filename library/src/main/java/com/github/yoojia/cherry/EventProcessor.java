package com.github.yoojia.cherry;

/**
 * 事件处理器。
 * 在事件被拦截处理器处理后执行，可以对事件进行转换处理，并返回。
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface EventProcessor {

    /**
     * 处理事件并返回
     * @param context Context
     * @param event 事件
     * @return 返回处理后的事件
     */
    Event process(Context context, Event event);
}
