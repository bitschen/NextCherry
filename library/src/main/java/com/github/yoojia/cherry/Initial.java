package com.github.yoojia.cherry;

import com.parkingwang.lang.data.Config;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
interface Initial {

    void onInit(Context context, Config args) throws Exception;
}
