package com.github.yoojia.cherry;

import com.parkingwang.lang.data.Config;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class InitialContext implements Initial {

    private final Config mArgs = new Config();
    private Context mContext;

    @Override
    public void onInit(Context context, Config args) throws Exception{
        mContext = context;
        mArgs.copyFrom(args);
    }

    final protected Config getArgs() {
        return mArgs;
    }

    final protected Context getContext() {
        if (mContext == null) {
            throw new IllegalStateException("Method<OnInit> MUST CALL super");
        }
        return mContext;
    }

}
