package com.github.yoojia.cherry;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
interface Lifecycle extends Initial {

    void onStart();

    void onStop();
}
