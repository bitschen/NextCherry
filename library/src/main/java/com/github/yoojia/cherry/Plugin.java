package com.github.yoojia.cherry;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public interface Plugin extends Lifecycle {}
