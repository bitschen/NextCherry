package com.github.yoojia.cherry;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface VirtualDevice extends Plugin{

    void setUUID(String uuid);
    String getUUID();

    void handleEvent(Context context, Event event);
}
