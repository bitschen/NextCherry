package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.support.VirtualSwitchDevice;
import com.github.yoojia.cherry.value.BoolValue;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class ConsoleOutputDevice extends VirtualSwitchDevice {

    @Override
    protected void handleEvent(Context context, Event event, BoolValue value) {
        System.out.println("ConsoleOutputDevice, set to: " + (value.bool ? "ON" : "OFF"));
    }
}
