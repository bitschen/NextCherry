package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.EventProcessor;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class ConvertEventToRemoteProcessor implements EventProcessor{

    /**
     * 无论从本地设备，还是Handler或者其它方式，通过调用 Context.postEvent 的消息，都会通过 Processor 处理链。
     */
    @Override
    public Event process(Context context, Event event) {
        // 只转换本地事件，远程远程不做转换（远程事件已封RemoteTarget标记，如果再包装为远程事件，会导致循环）
        if (event.options().mqttRemote()){
            return event;
        }else{
            return Event.wrapRemoteTarget(event);
        }
    }
}
