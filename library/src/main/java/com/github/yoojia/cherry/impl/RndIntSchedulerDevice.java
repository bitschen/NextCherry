package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.support.VirtualSchedulerDevice;
import com.github.yoojia.cherry.value.IntValue;

import java.util.Random;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class RndIntSchedulerDevice extends VirtualSchedulerDevice {

    private final Random mRandom = new Random();

    @Override
    protected void onSchedule() {
        getContext().postEvent(Event.createNoneTarget(
                IntValue.create(mRandom.nextInt(1000)).getBytes(),
                TOPIC,
                getUUID()));
    }
}
