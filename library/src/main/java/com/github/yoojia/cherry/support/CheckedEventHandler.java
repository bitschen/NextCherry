package com.github.yoojia.cherry.support;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.EventHandler;
import com.github.yoojia.cherry.InitialContext;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class CheckedEventHandler extends InitialContext implements EventHandler {

    @Override
    final public void onEvent(Context context, Event event) {
        if (check(event)){
            handleEvent(context, event);
        }
    }

    protected abstract boolean check(Event event);

    protected abstract void handleEvent(Context context, Event event);

}
