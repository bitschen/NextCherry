package com.github.yoojia.cherry.support;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.EventInterceptor;
import com.github.yoojia.cherry.InitialContext;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class CheckedEventInterceptor extends InitialContext implements EventInterceptor {

    @Override
    final public boolean onEvent(Context context, Event event) {
        if (onCheck(event)){
            return handleEvent(context, event);
        }else{
            return true;
        }
    }

    protected abstract boolean onCheck(Event event);
    protected abstract boolean handleEvent(Context context, Event event);

}
