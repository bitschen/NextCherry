package com.github.yoojia.cherry.support;

import com.github.yoojia.cherry.Event;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public abstract class RemoteEventHandler extends CheckedEventHandler {

    @Override
    protected boolean check(Event event) {
        return event.options()
                .mqttRemote();
    }

}
