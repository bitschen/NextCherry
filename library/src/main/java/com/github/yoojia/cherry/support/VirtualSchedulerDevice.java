package com.github.yoojia.cherry.support;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.parkingwang.lang.data.Config;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class VirtualSchedulerDevice extends AbstractVirtualDevice {

    public static final String TOPIC = createTopic("schedulers");

    private final Timer mTimer = new Timer("SchedulerDevice", true);

    @Override
    public void onInit(Context context, Config args) throws Exception {
        super.onInit(context, args);
    }

    @Override
    public void onStart() {
        super.onStart();
        final long period = getArgs().getInt("period", 1000);
        final long delay = getArgs().getInt("delay", 0);
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                onSchedule();
            }
        }, delay, period);
    }

    @Override
    public void onStop() {
        super.onStop();
        mTimer.purge();
        mTimer.cancel();
    }

    @Override
    public void handleEvent(Context context, Event event) {
        // nop
    }

    protected abstract void onSchedule();
}
