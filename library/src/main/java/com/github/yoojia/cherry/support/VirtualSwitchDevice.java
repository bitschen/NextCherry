package com.github.yoojia.cherry.support;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.value.BoolValue;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class VirtualSwitchDevice extends AbstractVirtualDevice {

    public static final String TOPIC = Context.TOPIC_SWITCH;

    @Override
    final public void handleEvent(Context context, Event event) {
        final BoolValue value = event.valueOfBool();
        handleEvent(context, event, value);
    }

    protected abstract void handleEvent(Context context, Event event, BoolValue value);
}
