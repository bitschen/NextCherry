package com.github.yoojia.cherry.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class AppPaths {

    public static Path pathOf(String path){
        return Paths.get(System.getProperty("user.dir"), path);
    }
}