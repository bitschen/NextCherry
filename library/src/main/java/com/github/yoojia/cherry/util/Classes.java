package com.github.yoojia.cherry.util;

import com.parkingwang.lang.Try;

/**
 * @author Yoojia Chen (yoojiachen@gmail.com)
 * @since 1.0
 */
public class Classes {

    @SuppressWarnings("unchecked")
    public static <T> T newInstance(final String className){
        return Try.die(()->
                (T)Classes.class.getClassLoader().loadClass(className).newInstance());
    }

    public static String resolveImplClass(String className){
        if (className.contains(".")) {
            return className;
        }else{
            return "com.github.yoojia.cherry.impl." + className;
        }
    }

}
