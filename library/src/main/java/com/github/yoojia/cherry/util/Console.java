package com.github.yoojia.cherry.util;

public class Console {

    protected boolean mExiting = false;

    public synchronized Console promptForExit(){
        System.out.println("PRESS Ctrl+C TO EXIST");
        mExiting = false;
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                mExiting = true;
            }
        });
        return this;
    }

    public void waitForExit() throws InterruptedException {
        while(!mExiting){
            Thread.sleep(50);
        }
    }

    public synchronized boolean exiting(){
        return mExiting;
    }

    public synchronized boolean isRunning(){
        return !mExiting;
    }
}