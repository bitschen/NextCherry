package com.github.yoojia.cherry.value;

import com.parkingwang.lang.kit.ByteKit;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public final class BoolValue extends FrameValue {

    public final boolean bool;

    public BoolValue(boolean bool) {
        super(ByteKit.fromInt(bool ? 1 : 0));
        this.bool = bool;
    }

    public BoolValue(byte[] bytes) {
        super(bytes);
        this.bool = ByteKit.toInt(bytes) > 0;
    }

    //////

    public static BoolValue create(byte[] intBytes){
        return new BoolValue(intBytes);
    }

    public static BoolValue create(boolean bool){
        return new BoolValue(bool);
    }

    public static BoolValue create(int value){
        return create(value >= 0);
    }

    //////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoolValue that = (BoolValue) o;
        return Objects.equals(this.bool, that.bool);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bool);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("bool = " + bool)
                .toString();
    }
}
