package com.github.yoojia.cherry.value;

import com.parkingwang.lang.kit.ByteKit;

import java.util.Objects;
import java.util.StringJoiner;

import static com.parkingwang.lang.kit.ByteKit.fromFloat;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public final class FloatValue extends FrameValue {

    public final float float1;
    public final float float2;
    public final float float3;

    public FloatValue(byte[] bytes) {
        super(bytes);
        this.float1 = floatOf(bytes, 0);
        this.float2 = floatOf(bytes, 1);
        this.float3 = floatOf(bytes, 2);
    }

    public FloatValue(float float1, float float2, float float3) {
        super(bytesOf(fromFloat(float1), fromFloat(float2), fromFloat(float3)));
        this.float1 = float1;
        this.float2 = float2;
        this.float3 = float3;
    }

    ////

    public static FloatValue create(byte[] bytes){
        return new FloatValue(bytes);
    }

    public static FloatValue create(float value1, float value2, float value3){
        return new FloatValue(value1, value2, value3);
    }

    public static FloatValue create(float value1, float value2){
        return create(value1, value2, 0);
    }

    public static FloatValue create(float value){
        return create(value, 0, 0);
    }

    private static float floatOf(byte[] bytes, int index){
        final byte[] b = getBytesOf(bytes, index);
        return b.length > 0 ? ByteKit.toFloat(b) : 0;
    }

    ////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FloatValue that = (FloatValue) o;
        return Objects.equals(this.float1, that.float1) &&
                Objects.equals(this.float2, that.float2) &&
                Objects.equals(this.float3, that.float3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(float1, float2, float3);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("float1 = " + float1)
                .add("float2 = " + float2)
                .add("float3 = " + float3)
                .toString();
    }

}
