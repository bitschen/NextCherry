package com.github.yoojia.cherry.value;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class FrameValue {
    
    private final byte[] mBytes;

    public FrameValue(byte[] bytes){
        this.mBytes = bytes;
    }
    
    public byte[] getBytes(){
        return mBytes;
    }

    public static FrameValue create(byte[] bytes){
        return new FrameValue(bytes);
    }

    ////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrameValue that = (FrameValue) o;
        return Objects.equals(this.mBytes, that.mBytes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mBytes);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("bytes = " + mBytes)
                .toString();
    }

    ////

    static byte[] getBytesOf(byte[] bytes, int index){
        if (bytes.length < 4){
            return new byte[0];
        }else{
            final int startPos = index * 4;
            if (startPos < bytes.length){
                final byte[] output = new byte[4];
                System.arraycopy(bytes, startPos, output, 0, 4);
                return output;
            }else{
                return new byte[0];
            }
        }
    }

    static byte[] bytesOf(byte[] b1, byte[] b2, byte[] b3){
        final byte[] bytes = new byte[4 * 3];
        System.arraycopy(b1, 0, bytes, 0, 4);
        System.arraycopy(b2, 0, bytes, 4, 4);
        System.arraycopy(b3, 0, bytes, 8, 4);
        return bytes;
    }
}
