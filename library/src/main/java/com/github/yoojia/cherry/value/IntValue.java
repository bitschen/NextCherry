package com.github.yoojia.cherry.value;

import com.parkingwang.lang.kit.ByteKit;

import java.util.Objects;
import java.util.StringJoiner;

import static com.parkingwang.lang.kit.ByteKit.fromInt;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public final class IntValue extends FrameValue {

    public final int int1;
    public final int int2;
    public final int int3;
    
    public IntValue(byte[] bytes){
        super(bytes);
        this.int1 = intOf(bytes, 0);
        this.int2 = intOf(bytes, 1);
        this.int3 = intOf(bytes, 2);
    }

    public IntValue(int int1, int int2, int int3) {
        super(bytesOf(fromInt(int1), fromInt(int2), fromInt(int3)));
        this.int1 = int1;
        this.int2 = int2;
        this.int3 = int3;
    }

    public static IntValue create(byte[] bytes){
        return new IntValue(bytes);
    }
    
    public static IntValue create(int value1, int value2, int value3){
        return new IntValue(value1, value2, value3);
    }

    public static IntValue create(int value1, int value2){
        return create(value1, value2, 0);
    }

    public static IntValue create(int value){
        return create(value, 0, 0);
    }

    private static int intOf(byte[] bytes, int index){
        final byte[] b = getBytesOf(bytes, index);
        return b.length > 0 ? ByteKit.toInt(b) : 0;
    }

    //////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntValue that = (IntValue) o;
        return Objects.equals(this.int1, that.int1) &&
                Objects.equals(this.int2, that.int2) &&
                Objects.equals(this.int3, that.int3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(int1, int2, int3);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("int1 = " + int1)
                .add("int2 = " + int2)
                .add("int3 = " + int3)
                .toString();
    }

}
