package com.github.yoojia.cherry.value;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public final class StringValue extends FrameValue {

    private final String mText;

    public StringValue(String text) {
        super(text.getBytes());
        this.mText = text;
    }

    public StringValue(byte[] bytes) {
        super(bytes);
        this.mText = null;
    }

    public String getText(){
        if (mText != null) {
            return mText;
        }else{
            return new String(getBytes());
        }
    }

    ////

    public static StringValue create(byte[] bytes){
        return new StringValue(bytes);
    }

    public static StringValue create(String value){
        return new StringValue(value);
    }

    ////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringValue that = (StringValue) o;
        return Objects.equals(this.mText, that.mText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mText);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("text = " + mText)
                .toString();
    }
}
