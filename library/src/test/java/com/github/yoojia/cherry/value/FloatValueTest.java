package com.github.yoojia.cherry.value;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class FloatValueTest {

    private static final Random RANDOM = new Random();

    @Test
    public void test1Value(){
        float float1 = RANDOM.nextFloat();
        FloatValue obj = FloatValue.create(FloatValue.create(float1).getBytes());
        Assert.assertEquals(float1, obj.float1, 0.00001);
    }

    @Test
    public void test2Value(){
        float float1 = RANDOM.nextFloat();
        float float2 = RANDOM.nextFloat();
        FloatValue obj = FloatValue.create(FloatValue.create(float1, float2).getBytes());
        Assert.assertEquals(float1, obj.float1, 0.00001);
        Assert.assertEquals(float2, obj.float2, 0.00001);
    }

    @Test
    public void test3Value(){
        float float1 = RANDOM.nextFloat();
        float float2 = RANDOM.nextFloat();
        float float3 = RANDOM.nextFloat();
        FloatValue obj = FloatValue.create(FloatValue.create(float1, float2, float3).getBytes());
        Assert.assertEquals(float1, obj.float1, 0.00001);
        Assert.assertEquals(float2, obj.float2, 0.00001);
        Assert.assertEquals(float3, obj.float3, 0.00001);
    }
}
