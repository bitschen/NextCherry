package com.github.yoojia.cherry.value;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class IntValueTest {

    public static final Random RANDOM = new Random();

    @Test
    public void test1Value(){
        int int1 = RANDOM.nextInt();
        IntValue obj = IntValue.create(IntValue.create(int1).getBytes());
        Assert.assertEquals(int1, obj.int1);
    }

    @Test
    public void test2Value(){
        int int1 = RANDOM.nextInt();
        int int2 = RANDOM.nextInt();
        IntValue obj = IntValue.create(IntValue.create(int1, int2).getBytes());
        Assert.assertEquals(int1, obj.int1);
        Assert.assertEquals(int2, obj.int2);
    }

    @Test
    public void test3Value(){
        int int1 = RANDOM.nextInt();
        int int2 = RANDOM.nextInt();
        int int3 = RANDOM.nextInt();
        IntValue obj = IntValue.create(IntValue.create(int1, int2, int3).getBytes());
        Assert.assertEquals(int1, obj.int1);
        Assert.assertEquals(int2, obj.int2);
        Assert.assertEquals(int3, obj.int3);
    }
}
