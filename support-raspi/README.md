# 树莓派板载设备支持库

## 虚拟设备列表

### PiGPIOControllerPlugin - GPIO板载控制器插件

> 全名：com.github.yoojia.cherry.impl.PiGPIOControllerPlugin

**注意**

使用树莓派板载设备支持库，必须注册此插件来完成GPIO接口的初始化和销毁操作。

----

#### PWN参数配置

在GPIO控制器初始化时，会同时设置PWN接口的全局参数。

- `pwm-mode`: String，脉冲模式，有以下类型：
    * `MARKSPACE`
    * `BALANCE`
- `pwm-range`: Int, 脉冲范围，默认 1024；
- `pwm-clock`: Int, 脉冲时钟，默认 100；

----

### PiSwitchDigitInputDevice - 输入型开关设备

> 设备全名：com.github.yoojia.cherry.impl.PiSwitchDigitInputDevice

配置参数：

- `pin-address`：Int, 板载的GPIO地址；
- `pull-state`: String，监听输入接口的电平变化方式，有以下类型：
    * `PULLUP` GPIO接口的电平被拉升为高电平，默认值；
    * `PULLDOWN` GPIO接口的电平被拉低为低电平；
    * `OFF` GPIO接口的电平被设置为OFF状态；

返回事件的数值类型：

> BoolValue

----

### PiSwitchDigitOutputDevice - 输出型开关设备

> 设备全名：com.github.yoojia.cherry.impl.PiSwitchDigitOutputDevice

配置参数：

- `pin-address`：Int, 板载的GPIO地址；
- `pull-state`: String，初始化时接口的电平，有以下类型：
    * `LOW` 低电平，默认值；
    * `HIGH` 高电平；
- `shutdown-state` 设备被关闭时重围GPIO地址的电平值；类型与`pull-state`一致。

触发事件的数值类型：

> BoolValue

----

### PiPWMDevice - 硬脉冲输出设备

> 设备全名：com.github.yoojia.cherry.impl.PiPWMDevice

配置参数：

- `pin-address`：Int, 板载的GPIO地址；
- `def-rate`: Int，初始化时接口的脉冲频率；默认为 1024

触发事件的数值类型：

> IntValue.int1

----

### PiSerialComDevice - 串口设备

> 设备全名：com.github.yoojia.cherry.impl.PiSerialComDevice

配置参数：

- `port-address`: String，串口地址。
- `baud-rate`: Int，默认 38400。
- `data-bits`: Int，默认 8。
- `stop-bits`: Int，默认 1。
- `parity`
- `flow-control`

触发事件的数值类型：

> FrameValue

----

### PiTxtReaderSerialComDevice - 文本类串口读取器

> 设备全名: com.github.yoojia.cherry.impl.PiTxtReaderSerialComDevice

返回事件的数值类型：

> FrameValue

----

## 配置参考

```yaml
devices:
  - class: "DigitOutputSwitchDevice"
    uuid: "raspberry.output.led#0"
    enabled: true
    args:
      debug: true
      pin-address: 0
      pin-state: "LOW"
      shutdown-state: "LOW"

  - class: "PiPWMDevice"
    uuid: "raspberry.output.speaker#24"
    enabled: true
    args:
      debug: true
      pin-address: 24
      pwm-def-rate: 0
      pwm-range: 1024
      pwm-mode: "markspace"
      pwm-clock: 100

  - class: "PiSwitchDigitInputDevice"
    uuid: "raspberry.input.button#21"
    enabled: true
    args:
      debug: true
      pin-address: 21
      pull-state: "pullup"


  - class: ".PiTxtReaderSerialComDevice"
    uuid: "raspberry.input.read#0"
    enabled: true
    args:
      debug: true
      port-address: "/dev/ttyAMA0"
      baud-rate: 38400
      data-bits: 8
      stop-bits: 1
      parity: "NONE"
      flow-control: "NONE"

# 插件
plugins:
  - class: "PiGPIOControllerPlugin"
    enabled: true

```