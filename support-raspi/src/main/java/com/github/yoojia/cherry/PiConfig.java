package com.github.yoojia.cherry;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public interface PiConfig {

    String NAME_PORT_ADDRESS = "port-address";
    String NAME_PIN_ADDRESS = "pin-address";
    String NAME_PIN_STATE = "pin-state";
    String NAME_SHUTDOWN_STATE = "shutdown-state";

    String NAME_PULL_STATE = "pull-state";

    String NAME_PWM_DEF_RATE = "def-rate";
    String NAME_PWM_RANGE = "pwm-range";
    String NAME_PWM_MODE = "pwm-mode";
    String NAME_PWM_CLOCK = "pwm-clock";

    String STATE_HIGH = "HIGH";
    String STATE_LOW = "LOW";

    String STATE_PULLDOWN = "PULLDOWN";
    String STATE_PULLUP = "PULLUP";
    String STATE_PULLOFF = "OFF";

    String PWM_BAL = "BALANCED";
    String PWM_MS = "MARKSPACE";

}
