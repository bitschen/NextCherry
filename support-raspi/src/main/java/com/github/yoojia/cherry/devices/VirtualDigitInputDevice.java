package com.github.yoojia.cherry.devices;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.PiConfig;
import com.parkingwang.lang.data.Config;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.yoojia.cherry.util.ConfigKit.checkRequired;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public abstract class VirtualDigitInputDevice extends AbstractVirtualDevice {

    private static final String TAG = VirtualDigitInputDevice.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(TAG);

    private final GpioPinListenerDigital mStateListener = (this::handleStateChanged);

    @Override
    public void onInit( Context context,  Config args) throws Exception {
        super.onInit(context, args);
        checkRequired(args, PiConfig.NAME_PIN_ADDRESS, TAG);
        checkRequired(args, PiConfig.NAME_PULL_STATE, TAG);

        final int address = args.getInt(PiConfig.NAME_PIN_ADDRESS);
        final String state = args.getString(PiConfig.NAME_PULL_STATE).toUpperCase();
        LOGGER.info("Init InputDigitDevice: uuid={}, address={}, listenState={}", getUUID(), address, state);

        final GpioPinDigitalInput inputPin = GpioFactory
                    .getInstance()
                    .provisionDigitalInputPin(
                            RaspiPin.getPinByAddress(address),
                            getPullState(state));
        if (inputPin == null) {
            throw new IllegalStateException("Failed to bind Input Pin: " + address);
        }
        // 监控开关状态变化
        inputPin.addListener(mStateListener);
    }

    @Override
    public void handleEvent( Context context,  Event event) {
        // 输入信号型开关不接受事件控制
        throw new UnsupportedOperationException(msgWithId("Method handleEvent(..) is not supported by default!"));
    }

    protected abstract void handleStateChanged(GpioPinDigitalStateChangeEvent state);

    ////

    private static PinPullResistance getPullState(String state){
        switch (state){
            default:
            case PiConfig.STATE_PULLUP: return PinPullResistance.PULL_UP;
            case PiConfig.STATE_PULLDOWN: return PinPullResistance.PULL_DOWN;
            case PiConfig.STATE_PULLOFF: return PinPullResistance.OFF;
        }
    }

}
