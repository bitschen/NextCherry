package com.github.yoojia.cherry.devices;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.PiConfig;
import com.parkingwang.lang.data.Config;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.yoojia.cherry.util.ConfigKit.checkRequired;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public abstract class VirtualDigitOutputDevice extends AbstractVirtualDevice {

    private static final String TAG = VirtualDigitOutputDevice.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(VirtualDigitOutputDevice.class);

    private GpioPinDigitalOutput mDigitPin;

    @Override
    public void onInit(Context context, Config args) throws Exception {
        super.onInit(context, args);
        checkRequired(args, PiConfig.NAME_PIN_ADDRESS, TAG);
        checkRequired(args, PiConfig.NAME_PIN_STATE, TAG);

        final int address = args.getInt(PiConfig.NAME_PIN_ADDRESS);
        final String state = args.getString(PiConfig.NAME_PIN_STATE).toUpperCase();
        final String shutdownState = args.getString(PiConfig.NAME_SHUTDOWN_STATE).toUpperCase();
        LOGGER.info("Init OutputDigitDevice, UUID: {}, Address: {}, DefState: {}, ShutdownState: {}",
                getUUID(), address, state, shutdownState);

        mDigitPin = GpioFactory.getInstance()
                .provisionDigitalOutputPin(
                        RaspiPin.getPinByAddress(address),
                        getUUID(),
                        (PiConfig.STATE_LOW.equals(state)) ?
                                PinState.LOW :
                                PinState.HIGH);

        if (mDigitPin == null) {
            throw new IllegalStateException("Fail to bind Output pin: " + address);
        }

        mDigitPin.setShutdownOptions(true,
                (PiConfig.STATE_LOW.equals(state)) ? PinState.LOW : PinState.HIGH);
    }

    protected GpioPinDigitalOutput getDigitPin() {
        return mDigitPin;
    }

    public void setHigh(){
        mDigitPin.high();
    }

    public void setLow(){
        mDigitPin.low();
    }

    public void toggle(){
        mDigitPin.toggle();
    }
}
