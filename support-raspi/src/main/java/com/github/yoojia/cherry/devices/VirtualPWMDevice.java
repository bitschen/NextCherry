package com.github.yoojia.cherry.devices;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.PiConfig;
import com.parkingwang.lang.data.Config;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.RaspiPin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.yoojia.cherry.util.ConfigKit.checkRequired;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public abstract class VirtualPWMDevice extends AbstractVirtualDevice {

    private static final String TAG = VirtualDigitOutputDevice.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(VirtualDigitOutputDevice.class);

    private GpioPinPwmOutput mPWMPin;

    @Override
    public void onInit(Context context, Config args) throws Exception {
        super.onInit(context, args);
        checkRequired(args, PiConfig.NAME_PIN_ADDRESS, TAG);

        final int address = args.getInt(PiConfig.NAME_PIN_ADDRESS);
        final int defPWMRate = args.getInt(PiConfig.NAME_PWM_DEF_RATE, 1024);

        LOGGER.info("Init OutputPWMDevice, UUID: {}, Address: {}, DefPWMRate",
                getUUID(),
                address,
                defPWMRate);

        mPWMPin = GpioFactory.getInstance()
                .provisionPwmOutputPin(
                        RaspiPin.getPinByAddress(address),
                        getUUID(),
                        defPWMRate);

        if (mPWMPin == null) {
            throw new IllegalStateException("Fail to bind PWM Output pin: " + address);
        }
    }

    public void setPWM(int pwm){
        mPWMPin.setPwm(pwm);
    }

    public void setPWMRage(int rage){
        mPWMPin.setPwmRange(rage);
    }

    public GpioPinPwmOutput getPWMPin() {
        return mPWMPin;
    }
}
