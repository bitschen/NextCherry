package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.devices.VirtualPWMDevice;
import com.github.yoojia.cherry.value.IntValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class PiPWMDevice extends VirtualPWMDevice {

    private static final Logger LOGGER = LoggerFactory.getLogger(PiPWMDevice.class);

    @Override
    public void handleEvent(Context context, Event event) {
        final IntValue value = event.valueOfInt();
        final int pwn = value.int1;
        setPWM(pwn);
        if (isLogging()){
            LOGGER.debug("Handling, uuid: {}, PWM: {} ", getUUID(), pwn);
        }
    }
}
