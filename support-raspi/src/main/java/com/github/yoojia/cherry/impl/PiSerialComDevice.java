package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.AbstractVirtualDevice;
import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.PiConfig;
import com.github.yoojia.cherry.value.FrameValue;
import com.parkingwang.lang.data.Config;
import com.pi4j.io.serial.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class PiSerialComDevice extends AbstractVirtualDevice {

    private static final Logger LOGGER = LoggerFactory.getLogger(PiSerialComDevice.class);

    public static final String TOPIC = createTopic("serial-com");

    private Serial mSerial;
    private SerialConfig mConfig;

    @Override
    public void onInit(Context context, Config args) throws Exception{
        super.onInit(context, args);
        mSerial = SerialFactory.createInstance();
        mConfig = new SerialConfig();
        final String portAddress;
        try {
            portAddress = args.getString(PiConfig.NAME_PORT_ADDRESS, SerialPort.getDefaultPort());
        } catch (Exception e) {
            final String message = msgWithId("Failed to get default SerialCom Port");
            LOGGER.error(message, e);
            throw new RuntimeException(message);
        }
        final int baudRate = args.getInt("baud-rate", Baud._38400.getValue());
        final int dataBits = args.getInt("data-bits", DataBits._8.getValue());
        final int stopBits = args.getInt("stop-bits", StopBits._1.getValue());
        final String parity = args.getString("parity", Parity.NONE.name());
        final String flowCtrl = args.getString("flow-control", FlowControl.NONE.name());

        LOGGER.debug("Init PiSerialComDevice: baud={}, dataBits={}, parity={}, stopBits={}, flowCtrl={}",
                baudRate, dataBits, parity, stopBits, flowCtrl);

        mConfig.device(portAddress)
                .baud(Baud.getInstance(baudRate))
                .dataBits(DataBits.getInstance(dataBits))
                .parity(Parity.getInstance(parity))
                .stopBits(StopBits.getInstance(stopBits))
                .flowControl(FlowControl.getInstance(flowCtrl));
    }

    @Override
    public void onStart() {
        try {
            mSerial.open(mConfig);
        } catch (IOException e) {
            LOGGER.error(msgWithId("Failed to OPEN PiSerialComDevice"), e);
        }
        // 接收串口数据
        mSerial.addListener(this::onReceivedData);
    }

    protected void onReceivedData(SerialDataEvent data){
        FrameValue frame;
        try {
            frame = FrameValue.create(data.getBytes());
        } catch (IOException e) {
            LOGGER.error(msgWithId("Error when received data from PiSerialComDevice"), e);
            return;
        }
        postEvent(Event.createNoneTarget(
                frame.getBytes(),
                TOPIC,
                getUUID()));
    }

    @Override
    public void onStop() {
        if (mSerial != null && mSerial.isOpen()) {
            try {
                mSerial.close();
            } catch (IOException e) {
                LOGGER.error(msgWithId("Failed to CLOSE PiSerialComDevice"), e);
            }
        }
    }

    @Override
    public void handleEvent(Context context, Event event) {
        // 接收到数据时，将数据写入串口
        final FrameValue frame = event.valueOfFrame();
        if (mSerial.isOpen()){
            try {
                mSerial.write(frame.getBytes());
            } catch (IOException e) {
                LOGGER.error(msgWithId("Error when write frame data to PiSerialComDevice"), e);
            }
        }
    }
}
