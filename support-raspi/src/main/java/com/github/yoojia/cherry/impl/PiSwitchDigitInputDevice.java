package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.PiConfig;
import com.github.yoojia.cherry.devices.VirtualDigitInputDevice;
import com.github.yoojia.cherry.value.BoolValue;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数字信号开关型输入设备
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class PiSwitchDigitInputDevice extends VirtualDigitInputDevice {

    private static final Logger LOGGER = LoggerFactory.getLogger(PiSwitchDigitInputDevice.class);

    public static final String TOPIC = topicAppend(Context.TOPIC_SWITCH, "digit-input");

    @Override
    protected void handleStateChanged(GpioPinDigitalStateChangeEvent state) {
        final boolean isHigh = state.getState().isHigh();
        final Event evt = Event.createNoneTarget(
                BoolValue.create(isHigh).getBytes(),
                TOPIC,
                getUUID());
        if (isLogging()){
            LOGGER.debug("Changed, uuid: {}, beState: {}",
                    getUUID(),
                    (isHigh ? PiConfig.STATE_HIGH : PiConfig.STATE_LOW));
        }
        postEvent(evt);
    }


}
