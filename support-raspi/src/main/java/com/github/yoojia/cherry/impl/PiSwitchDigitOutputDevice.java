package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Context;
import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.devices.VirtualDigitOutputDevice;
import com.github.yoojia.cherry.value.BoolValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数字信号开关型设备
 * @author 陈小锅 (yoojiachen@gmail.com)
 * @since 1.0.0
 */
public class PiSwitchDigitOutputDevice extends VirtualDigitOutputDevice {

    public static final String TOPIC = topicAppend(Context.TOPIC_SWITCH, "digit-output");

    private static final Logger LOGGER = LoggerFactory.getLogger(PiSwitchDigitOutputDevice.class);

    @Override
    public void handleEvent(Context context, Event event) {
        final BoolValue state = event.valueOfBool();
        if (state.bool){
            setHigh();
        }else{
            setLow();
        }
        if (isLogging()){
            LOGGER.debug("Handling, uuid: {}, setToState: {}", getUUID(), (state.bool ? "High" : "Low"));
        }
    }
}
