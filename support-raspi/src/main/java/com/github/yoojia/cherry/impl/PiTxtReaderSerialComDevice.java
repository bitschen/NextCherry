package com.github.yoojia.cherry.impl;

import com.github.yoojia.cherry.Event;
import com.github.yoojia.cherry.value.StringValue;
import com.pi4j.io.serial.SerialDataEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author 2017 Yoojia Chen (yoojiachen@gmail.com)
 */
public class PiTxtReaderSerialComDevice extends PiSerialComDevice {

    private static final Logger LOGGER = LoggerFactory.getLogger(PiSerialComDevice.class);

    @Override
    protected void onReceivedData(SerialDataEvent data) {
        StringValue value;
        try {
            value = StringValue.create(data.getBytes());
        } catch (IOException e) {
            LOGGER.error(msgWithId("Error when received data from PiTxtReaderSerialComDevice"), e);
            return;
        }
        postEvent(Event.createNoneTarget(
                value.getBytes(),
                TOPIC,
                getUUID()));
    }
}
